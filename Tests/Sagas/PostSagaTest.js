import FixtureAPI from '../../App/Services/FixtureApi'
import { put, call } from 'redux-saga/effects'
import { requestPosts } from '../../App/Containers/Posts/List/sagas'
import PostListActions from '../../App/Containers/Posts/List/redux';
import { path } from 'ramda'
 import api from '../../App/Services/Api'


const response = [1]

const getState = () => state

test('requestPosts Saga test', function(t) {
    const generator = requestPosts(api)
    
      let next = generator.next(PostListActions.getPostsRequest())
      t.deepEqual(next.value, call(api.getAllPosts), 'must yield api.getProducts')
    
      next = generator.next(response)
      t.deepEqual(next.value, put(PostListActions.getPostsSuccess(response)), 'must yield actions.receiveProducts(products)')
    
      t.end()
    })
  