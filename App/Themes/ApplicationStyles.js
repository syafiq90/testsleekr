import Fonts from './Fonts';
import Metrics from './Metrics';
import Colors from './Colors';

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: Colors.main
    },
    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    },
    container: {
      flex: 1,
      paddingTop: Metrics.baseMargin,
      backgroundColor: Colors.transparent
    },
    buttonStyle:{
      marginBottom: 20,
      color:Colors.snow,
      fontSize : 19
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.snow,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    subtitle: {
      color: Colors.snow,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin
    },
    titleText: {
      fontSize: 20,
      color: Colors.text,
      textAlign:'center'
    }
  },
  darkLabelContainer: {
    padding: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin
  },
  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.ember,
    alignItems: 'center',
    textAlign: 'center'
  },
  global: {
    strip: {
      width: 21,
      marginTop: 11,
      marginBottom: 11,
      borderTopWidth: 1,
      borderTopColor: Colors.darkGrey,
    },
  },
  list: {
    container: {
      display: 'flex',
      flexDirection: 'row',
      marginTop: Metrics.baseMargin,
      paddingBottom: Metrics.baseMargin,
      marginLeft: Metrics.doubleBaseMargin,
      marginRight: Metrics.doubleBaseMargin,
      borderBottomColor: Colors.medGrey,
      borderBottomWidth: Metrics.horizontalLineHeight,
    },
    image: {
      width: Metrics.listCircleSize,
      height: Metrics.listCircleSize,
      borderRadius: Metrics.listCircleRadius,
    },
    imageContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      marginRight:Metrics.doubleBaseMargin
    },
    metaContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      flex: 1,
      paddingTop: Metrics.baseMargin,
      paddingBottom: Metrics.baseMargin,
    },
    syncContainer: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    syncIcon: {
      width: Metrics.icons.small,
      height: Metrics.icons.small,
    },
    title: {
      fontSize: Metrics.listTitle,
      color: Colors.haraGreen,
    },
    content: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    text: {
      fontSize: Metrics.listContent,
      color: Colors.darkGrey,
    },
    icon: {
      width: Metrics.headerSubtitleIcon,
      height: Metrics.headerSubtitleIcon,
      marginRight: Metrics.baseMargin,
      resizeMode: 'contain',
    },
  },
  detailHeader: {
    headerMapOfflineContainer: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: "100%",
      height: Metrics.headerImageHeight,
      alignSelf:'flex-start',
      backgroundColor: Colors.medGrey,
    },
    headerMapOfflineIcon: {
      width: 108.2,
      height: 100,      
    },
    headerContainer: {
      backgroundColor: Colors.brokenWhite,
      padding: Metrics.doubleBaseMargin,
      marginBottom: Metrics.baseMargin
    },
    headerItemContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    headerTitleIcon: {
      width: Metrics.headerTitleIcon,
      height: Metrics.headerTitleIcon,
      marginRight: Metrics.baseMargin,
      resizeMode: 'contain',
    },
    headerTitleText: {
      fontSize: Metrics.headerTitleText,
      color: Colors.darkGrey,
      marginTop: Metrics.baseMargin,
      marginBottom: Metrics.smallMargin,
    },
    headerSubtitleIcon: {
      width: Metrics.headerSubtitleIcon,
      height: Metrics.headerSubtitleIcon,
      marginRight: Metrics.baseMargin,
      resizeMode: 'contain',
    },
    headerSubtitleText: {
      fontSize: Metrics.headerSubtitleText,
      color: Colors.darkGrey
    },
  },
  detailProperty: {
    propertyContainer:{
      paddingLeft:Metrics.doubleBaseMargin,
      paddingRight:Metrics.doubleBaseMargin,
      paddingTop:Metrics.baseMargin,
      marginBottom:Metrics.smallMargin,
    },
    propertyLabel:{
      fontSize:Metrics.propertyLabel,
      color:Colors.medGrey,
      marginBottom:Metrics.smallMargin,
    },
    propertyValue:{
      fontSize:Metrics.propertyValue,
      color:Colors.darkGrey,
    },
  },
  detailMenu: {
    menuContainer: {
      marginTop:Metrics.doubleBaseMargin,
      borderTopWidth: 1,
      borderTopColor: Colors.medGrey,
    },
    menuItem: {
      borderBottomWidth: 1,
      borderBottomColor: Colors.medGrey,
      paddingTop:Metrics.doubleBaseMargin,
      paddingBottom:Metrics.doubleBaseMargin,
      paddingLeft:Metrics.doubleBaseMargin,
      paddingRight:Metrics.doubleBaseMargin,
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
    },
    menuTxt: {
      color: Colors.haraGreen,
      fontSize: Metrics.menuText
    },
    menuIcon: {
      height: Metrics.menuIcon,
      resizeMode: 'contain',
    },
  }
};

export default ApplicationStyles;
