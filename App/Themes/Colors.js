const colors = {
  haraGreen: '#02B875',
  darkBlue: '#34465D',
  navy: '#33658A',
  skyBlue: '#C0E0DE',
  red: '#F26663',
  yellow: '#F8B716',
  softGrey: '#95989A',
  black: '#000',
  blackGrey: '#252525',
  darkGrey: '#757575',
  medGrey: '#C0C0C0',
  lightGrey: '#EAEAEA',
  brokenWhite: '#F1F1F1',
  white: '#FFF',
}

export default colors
