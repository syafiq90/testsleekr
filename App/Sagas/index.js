import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */


import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { PostListType } from '../Containers/Posts/List/redux'
import { PostDetailType } from '../Containers/Posts/Details/redux'
import { AlbumsListTypes } from '../Containers/Albums/List/redux'
import { AlbumsPhotosType } from '../Containers/Albums/Photos/redux'
import { TodosListTypes } from '../Containers/Todos/List/redux'
import { UsersListTypes } from '../Containers/Users/List/redux'
import { UsersTypes } from '../Containers/Users/Details/redux'



/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getUserAvatar } from './GithubSagas'
import { requestPosts,movePostDetails,requestFilterPosts } from '../Containers/Posts/List/sagas'
import { requestComments } from '../Containers/Posts/Details/sagas'
import { requestAlbums,moveAlbumsPhotos,requestFilterAlbums } from '../Containers/Albums/List/sagas'
import { requestPhotos } from '../Containers/Albums/Photos/sagas'

import { requestUsers,moveUserDetails } from '../Containers/Users/List/sagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),


    takeLatest(PostListType.GET_POSTS_REQUEST, requestPosts, api),
    takeLatest(PostListType.MOVE_TO_POST_DETAILS, movePostDetails, api),    
    takeLatest(PostListType.GET_FILTER_POSTS_REQUEST, requestFilterPosts, api),    
    takeLatest(PostDetailType.GET_COMMENTS_REQUEST, requestComments, api),    
    
    takeLatest(AlbumsListTypes.GET_ALBUMS_REQUEST, requestAlbums, api),
    takeLatest(AlbumsListTypes.MOVE_TO_PHOTOS, moveAlbumsPhotos, api),
    takeLatest(AlbumsListTypes.GET_FILTER_ALBUMS_REQUEST, requestFilterAlbums, api),
    takeLatest(AlbumsPhotosType.GET_PHOTOS_REQUEST, requestPhotos, api),
    

    takeLatest(UsersListTypes.GET_USERS_REQUEST, requestUsers, api),
    takeLatest(UsersListTypes.MOVE_TO_USER_DETAILS, moveUserDetails, api),
    
  ])
}
