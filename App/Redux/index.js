import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'
import ReduxPersist from '../Config/ReduxPersist'

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  github: require('./GithubRedux').reducer,
  search: require('./SearchRedux').reducer,

  //Albums
  AlbumsList:require('../Containers/Albums/List/redux').reducer,
  AlbumsPhotos:require('../Containers/Albums/Photos/redux').reducer,
  
  //Posts
  PostList:require('../Containers/Posts/List/redux').reducer,
  PostDetail:require('../Containers/Posts/Details/redux').reducer,
  
  //Todos
  Todos:require('../Containers/Todos/List/redux').reducer,
  
  //Users
  UserDetails:require('../Containers/Users/Details/redux').reducer,
  UsersList:require('../Containers/Users/List/redux').reducer
  
})

export default () => {
  let finalReducers = reducers
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig
    finalReducers = persistReducer(persistConfig, reducers)
  }

  let { store, sagasManager, sagaMiddleware } = configureStore(finalReducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}
