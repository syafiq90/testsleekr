import { UsersListTypes } from '../Containers/Users/List/redux'
import  UsersListActions from '../Containers/Users/List/redux'
import {create} from 'apisauce'

// import 'rxjs'
// import { Observable } from 'rxjs/Observable'
//  const send = ( ) => {
//     const api = create({
//       baseURL: 'https://jsonplaceholder.typicode.com/',
//      })
//     return (dispatch) => {
//        api
//        .get('todos')
//        .then((response) => { dispatch(Creators.getTodosSuccess(response.data))})
//        .then(console.log)
//     }
//   }

const fetchUserEpic = action$ =>
  action$.ofType(UsersListTypes.GET_USERS_REQUEST)
    .mergeMap(action =>
      Observable.fromPromise(getUsersAPI())
        .map(response => UsersListActions.getUsersSuccess(response))
        .catch(error => Observable.of(UsersListActions.getUsersFailed(response)))
      )

export default fetchUserEpic