import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getFilterAlbumsRequest: ['id'], 
  getAlbumsRequest: null,
  getAlbumsSuccess: ['response'],
  getAlbumsFailed: ['errors'],
  moveToPhotos:['item']

});

export const AlbumsListTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loading: false,
  errors: false,
  data:{ },
  filterByUsers:false
});

/* ------------- Reducers ------------- */


export const request = (state) => state.merge({
  loading: true,  
});

export const fetchSuccess = (state, { response }) => state.merge({
  data:response,
  loading: false,
});

export const fetchFailed = (state, { errors }) => state.merge({
  errors,
});



/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_FILTER_ALBUMS_REQUEST]: request,  
  [Types.GET_ALBUMS_REQUEST]: request,
  [Types.GET_ALBUMS_SUCCESS]: fetchSuccess,
  [Types.GET_ALBUMS_FAILED]: fetchFailed,
  [Types.MOVE_TO_PHOTOS]: null,
  
});


/* ------------- Selectors ------------- */
