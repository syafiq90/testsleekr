import React, { Component } from 'react'
import { ScrollView, Text,Image, KeyboardAvoidingView,FlatList ,TouchableOpacity,View } from 'react-native'
import { connect } from 'react-redux'
import AlbumsListActions from './redux';
import withSpinner from '../../../Components/WrappedPageLoading'
const ScrollViewWithSpinner = withSpinner(ScrollView);
import i18n from 'react-native-i18n';
import Images from '../../../Themes/Images'
import Colors from '../../../Themes/Colors'
import FilterByUsers from '../../../Components/FilterByUser'
import UsersListActions from '../../Users/List/redux';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './AlbumsScreenStyle'

class AlbumsScreen extends Component {
 
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params ? 
    navigation.state.params.filter ? i18n.t('Navigation.filterByUsers') :
    i18n.t('Navigation.albums'):i18n.t('Navigation.albums'),
    headerRight: <View style={{
      display: 'flex',
      flexDirection: 'row',
    }}>
      <TouchableOpacity onPress={() => {
          navigation.setParams({
            filter: true,
          });
        }}>
        <Image source={Images.sortIcon} style={{
          height: 25,
          width: 20,
          marginRight: 20,
          tintColor: Colors.darkGrey
        }} />
      </TouchableOpacity>
    </View>,
  });

  handleClickAll = async() =>{
    await this.props.dispatch(AlbumsListActions.getAlbumsRequest());
    await this.props.navigation.setParams({filter:false})
  }
  renderHeader = () =>
          <TouchableOpacity onPress={()=>{this.handleClickAll()}}>
            <View style={[styles.container]}>
            <View style={styles.metaContainer}>
            <Text style= {styles.title}>
            ALL
            </Text>
            </View>
            </View>
        </TouchableOpacity>;
  renderFilterItem= ({item}) =>
    <FilterByUsers
    name={item.name}
    onPress={() => this.handleFilter(item)}    
    />;
  handleFilter= async(item) =>{  
      await this.props.dispatch(AlbumsListActions.getFilterAlbumsRequest(item.id));     
      await this.props.navigation.setParams({filter:false})
  }
  handleClick= (item) =>{    
    this.props.dispatch(AlbumsListActions.moveToPhotos(item));    
  }


  renderItem= ({item}) =>{
    this.handleClick = this.handleClick.bind(this);
    
    
    return(<TouchableOpacity onPress={()=>this.handleClick(item)}>
      <View style={[styles.container]}>
          <View style={styles.metaContainer}>
            <Text numberOfLines= {1} style= {styles.title}>
              {item.title}
            </Text>
          </View> 
      </View>
    </TouchableOpacity>
  )};
  componentDidMount(){
    this.props.dispatch(AlbumsListActions.getAlbumsRequest());
    this.props.userList.data ? userList  : this.props.dispatch(UsersListActions.getUsersRequest());
    
  }
  render () {
    const { albums,loading,userList } = this.props;
    const filter=this.props.navigation.state.params ? this.props.navigation.state.params.filter : false ;
    
    return (
      <ScrollViewWithSpinner
      isLoading={loading}>
       { filter?
          (<View style={styles.mainContainer}>
            <FlatList
            data={userList} 
            ListHeaderComponent={this.renderHeader}            
            keyExtractor={(item) => item.id}
            renderItem={this.renderFilterItem}
            />
          </View>):
          (<View style={styles.mainContainer}>
            <FlatList
            data={albums} 
            keyExtractor={(item) => item.id}
            renderItem={this.renderItem}
            />
          </View>)
        }
      </ScrollViewWithSpinner>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    albums: state.AlbumsList.data,
    loading: state.AlbumsList.loading,
    userList: state.UsersList.data,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AlbumsScreen)
