import { call, put, select } from 'redux-saga/effects';
import AlbumsListActions from './redux';
import AlbumPhotosActions from '../Photos/redux';
import { NavigationActions } from 'react-navigation';

export function* requestAlbums (api ) {
    try {
      // Do the upload
      const response = yield call(api.getAllAlbums);
      // Remove uploading flag if upload success
       yield put(AlbumsListActions.getAlbumsSuccess(response.data));
    } catch (error) {
      // Send error log
       yield put(AlbumsListActions.getAlbumsFailed(error));
    }
  }

export function* requestFilterAlbums (api,{id} ) {
  try {
    // Do the upload
    const response = yield call(api.getFilterAlbums,id);
    // Remove uploading flag if upload success
     yield put(AlbumsListActions.getAlbumsSuccess(response.data));
  } catch (error) {
    // Send error log
     yield put(AlbumsListActions.getAlbumsFailed(error));
  }
}

  export function* moveAlbumsPhotos (api,{item} ) {
    try {
      yield put(AlbumPhotosActions.saveAlbums(item));
      yield put(NavigationActions.navigate({
        routeName:'PhotosScreen'
      }));
    } catch (error) {
      // Send error log
    }
}