import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getPhotosRequest: ['id'],
  getPhotosSuccess: ['response'],
  getPhotosFailed: ['errors'],
  saveAlbums: ['data']
  
  
});

export const AlbumsPhotosType = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loading: false,
  errors: false,
  dataAlbums:false,
  dataPhotos:[]
});

/* ------------- Reducers ------------- */


export const request = (state) => state.merge({
  loading: true,  
});

export const fetchSuccess = (state, { response }) => state.merge({
  dataPhotos:response,
  loading: false,
});

export const save = (state, { data }) => state.merge({
  dataAlbums:data,
  loading: false,
});

export const fetchFailed = (state, { errors }) => state.merge({
  errors,
});



/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_PHOTOS_REQUEST]: request,
  [Types.GET_PHOTOS_SUCCESS]: fetchSuccess,
  [Types.GET_PHOTOS_FAILED]: fetchFailed,
  [Types.SAVE_ALBUMS]:save,
  

});


/* ------------- Selectors ------------- */
