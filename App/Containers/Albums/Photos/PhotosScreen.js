import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, FlatList, Image, View,TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import AlbumsPhotosActions from './redux'
import PhotoGrid from 'react-native-photo-grid';
import withSpinner from '../../../Components/WrappedPageLoading'
const ScrollViewWithSpinner = withSpinner(ScrollView);
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './PhotosScreenStyle'

class PhotosScreen extends Component {
  
  componentDidMount(){
    const { albumsPhotos } = this.props;
    const id= albumsPhotos.dataAlbums? albumsPhotos.dataAlbums.id: ' '
    this.props.dispatch(AlbumsPhotosActions.getPhotosRequest(id));
    
  
  }

  renderItem(item, itemSize) {
    return(
      <TouchableOpacity
      key = { item.id }
      style = {{ width: itemSize, height: itemSize }}>
          <Image
                resizeMode = "cover"
                style = {{ flex: 1 }}
                source = {{ uri: item.thumbnailUrl }}
              />
      </TouchableOpacity>
    )
  }
  render () {
    const { albumsPhotos } = this.props;
    const dataPhotos= albumsPhotos.dataPhotos
    const loading= albumsPhotos.loading;
    console.log('dataPhotos',JSON.stringify(dataPhotos));
    return (
    
        <ScrollView style={styles.container}>
          <PhotoGrid
              data = { dataPhotos }
              itemsPerRow = { 3 }
              itemMargin = { 1 }
              renderItem = { this.renderItem }
        />
        </ScrollView>
   
    )
  }
}

const mapStateToProps = (state) => {
  return {
    albumsPhotos:state.AlbumsPhotos
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotosScreen)
