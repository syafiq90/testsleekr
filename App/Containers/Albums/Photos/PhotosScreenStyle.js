import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../../Themes/'
import Metrics from '../../../Themes/Metrics'
export default StyleSheet.create({
  ...ApplicationStyles.screen,
  item: {
    height: 150,
    backgroundColor: '#CCCCCC',
    padding: 10,
  },
  flex: {
    flex: 1,
  },
})
