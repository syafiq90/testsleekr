import { call, put, select } from 'redux-saga/effects';
import AlbumsPhotosActions from './redux';


export function* requestPhotos(api,{id} ) {
    try {
      // Do the upload
      const response = yield call(api.getAllPhotos,id);
      // Remove uploading flag if upload success
       yield put(AlbumsPhotosActions.getPhotosSuccess(response.data));
    } catch (error) {
      // Send error log
       yield put(AlbumsPhotosActions.getPhotosFailed(error));
    }
  }