import { call, put, select } from 'redux-saga/effects';
import PostListActions from './redux';
import PostDetailActions from '../Details/redux'
import { NavigationActions } from 'react-navigation';


export function* requestPosts (api) {
    
  
    try {
      // Do the upload
      const response = yield call(api.getAllPosts);
      // Remove uploading flag if upload success
       yield put(PostListActions.getPostsSuccess(response.data));
    } catch (error) {
      // Send error log
       yield put(PostListActions.getPostsFailed(error));
    }
  }


export function* requestFilterPosts (api,{id} ) {
  

  try {
    // Do the upload
    const response = yield call(api.getFilterPosts,id);
    // Remove uploading flag if upload success
     yield put(PostListActions.getPostsSuccess(response.data));
  } catch (error) {
    // Send error log
     yield put(PostListActions.getPostsFailed(error));
  }
}
  
export function* movePostDetails (api,{ item} ) {
    
  
    try {
      yield put(PostDetailActions.savePost(item));
      yield put(NavigationActions.navigate({
        routeName:'PostDetailsScreen'
      }));
    } catch (error) {
      // Send error log
       
    }
}