import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getFilterPostsRequest: ['id'],  
  getPostsRequest: null,
  getPostsSuccess: ['response'],
  getPostsFailed: ['errors'],
  moveToPostDetails:['item'],

  
});

export const PostListType = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loading: false,
  errors: false,
  data:{ },
  filterByUsers:false
});

/* ------------- Reducers ------------- */


export const request = (state) => state.merge({
  loading: true,  
});

export const fetchSuccess = (state, { response }) => state.merge({
  data:response,
  loading: false,
});

export const fetchFailed = (state, { errors }) => state.merge({
  errors,
});



/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_FILTER_POSTS_REQUEST]: request,  
  [Types.GET_POSTS_REQUEST]: request,
  [Types.GET_POSTS_SUCCESS]: fetchSuccess,
  [Types.GET_POSTS_FAILED]: fetchFailed,
  [Types.MOVE_TO_POST_DETAILS]: null,
  
  
});


/* ------------- Selectors ------------- */
export const callFilter = (state) => state.UsersList.filter;
