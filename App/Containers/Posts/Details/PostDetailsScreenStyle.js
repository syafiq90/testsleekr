import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../../Themes/'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  ...ApplicationStyles.detailHeader,
  ...ApplicationStyles.list,
  ...ApplicationStyles.detailProperty
})
