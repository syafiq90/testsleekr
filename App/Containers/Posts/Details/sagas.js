import { call, put, select } from 'redux-saga/effects';
import PostDetailActions from './redux';


export function* requestComments (api,{id} ) {
    
  
    try {
      
      // Do the upload
      const response = yield call(api.getAllComments,id);
      // Remove uploading flag if upload success
       yield put(PostDetailActions.getCommentsSuccess(response.data));
    } catch (error) {
      // Send error log
       yield put(PostDetailActions.getCommentsFailed(error));
    }
  }