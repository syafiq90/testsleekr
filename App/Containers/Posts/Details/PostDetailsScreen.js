import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView,View, FlatList,TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import PostDetailActions from './redux'
import withSpinner from '../../../Components/WrappedPageLoading'
const ScrollViewWithSpinner = withSpinner(ScrollView);
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './PostDetailsScreenStyle'

class PostDetailsScreen extends Component {
  componentDidMount(){
    const { postDetails } = this.props;   
    const id= postDetails.dataPost.id? postDetails.dataPost.id: ' '     
    this.props.dispatch(PostDetailActions.getCommentsRequest(id));
  }

  renderItem= ({item}) =>(
    
    <TouchableOpacity>
      <View style={[styles.container]}>
          <View style={styles.metaContainer}>
            <Text numberOfLines= {1} style= {styles.title}>
              {item.name}
            </Text>
            <View style= {[styles.content,{flexDirection:"column",alignItems:"flex-start", justifyContent:"flex-start"}]}>
              <Text style= {styles.Text}>
                {item.body}
              </Text>
              </View>
          </View> 
      </View>
    </TouchableOpacity>
  );
  render () {
    const { postDetails } = this.props;
    const loading= postDetails.loading;
    const dataPost= postDetails.dataPost.title? postDetails.dataPost: ' '
    const dataComments= postDetails.dataComments? postDetails.dataComments: ' '
    return (
      <ScrollViewWithSpinner
      isLoading={loading}>
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
            <Text>
              Title
            </Text>
            <Text style={[styles.headerTitleText]}>
              {dataPost.title}
            </Text>
            <Text>
              body
            </Text>
            <Text style={styles.headerSubtitleText}>
              {dataPost.body}
            </Text>
        </View>
        <View style={styles.propertyContainer}>
            <Text>
              Comments
            </Text>
          <FlatList
          data={dataComments} 
          keyExtractor={(item) => item.id}
          renderItem={(item)=>this.renderItem(item)}
        />
        </View>
      </View>
      </ScrollViewWithSpinner>

    )
  }
}

const mapStateToProps = (state) => {
  
  return {
    postDetails: state.PostDetail,
    
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostDetailsScreen)
