import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getCommentsRequest: ['id'],
  getCommentsSuccess: ['response'],
  getCommentsFailed: ['errors'],
  savePost: ['data']
  
  
});

export const PostDetailType = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loading: false,
  errors: false,
  dataComments:false,
  dataPost: { }
});

/* ------------- Reducers ------------- */


export const request = (state) => state.merge({
  loading: true,  
});

export const fetchSuccess = (state, { response }) => state.merge({
  dataComments:response,
  loading: false,
});

export const save = (state, { data }) => state.merge({
  dataPost:data,
  loading: false,
});

export const fetchFailed = (state, { errors }) => state.merge({
  errors,
});



/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_COMMENTS_REQUEST]: request,
  [Types.GET_COMMENTS_SUCCESS]: fetchSuccess,
  [Types.GET_COMMENTS_FAILED]: fetchFailed,
  [Types.SAVE_POST]:save,
  

});


/* ------------- Selectors ------------- */
