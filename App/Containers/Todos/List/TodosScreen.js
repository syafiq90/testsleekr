import React, { Component } from 'react'
import { ScrollView, Image,Text, KeyboardAvoidingView,FlatList,TouchableOpacity,View } from 'react-native'
import { connect } from 'react-redux'
import TodosActions, {send,requestFilterTodos} from './redux';
import UsersListActions from '../../Users/List/redux';
import withSpinner from '../../../Components/WrappedPageLoading'
const ScrollViewWithSpinner = withSpinner(ScrollView);
import i18n from 'react-native-i18n';
import Images from '../../../Themes/Images'
import Colors from '../../../Themes/Colors'
import FilterByUsers from '../../../Components/FilterByUser'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './TodosScreenStyle'

class TodosScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params ? 
    navigation.state.params.filter ? i18n.t('Navigation.filterByUsers') :
    i18n.t('Navigation.todos'):i18n.t('Navigation.todos'),
    headerRight: <View style={{
      display: 'flex',
      flexDirection: 'row',
    }}>
      <TouchableOpacity onPress={() => {
          navigation.setParams({
            filter: true,
          });
        }}>
        <Image source={Images.sortIcon} style={{
          height: 25,
          width: 20,
          marginRight: 20,
          tintColor: Colors.darkGrey
        }} />
      </TouchableOpacity>
    </View>,
  });


  handleClickAll = async() =>{
    await this.props.dispatch(send());
    await this.props.navigation.setParams({filter:false})
  }
  renderHeader = () =>
          <TouchableOpacity onPress={()=>{this.handleClickAll()}}>
            <View style={[styles.container]}>
            <View style={styles.metaContainer}>
            <Text style= {styles.title}>
            ALL
            </Text>
            </View>
            </View>
        </TouchableOpacity>;
  handleFilter= async(item) =>{  
    await this.props.dispatch(requestFilterTodos(item.id));     
    await this.props.navigation.setParams({filter:false})
  }
  renderFilterItem= ({item}) =>
  <FilterByUsers
  name={item.name}
  onPress={() => this.handleFilter(item)}    
  />;
  renderItem= ({item}) =>(
    <TouchableOpacity>
      <View style={[styles.container]}>
          <View style={styles.metaContainer}>
            <Text numberOfLines= {1} style= {styles.title}>
              {item.title}
            </Text>
            <View style= {styles.content}>
              <Text numberOfLines= {1} style= {styles.Text}>
                {item.completed}
              </Text>
              </View>
          </View> 
      </View>
    </TouchableOpacity>
  );

  componentDidMount(){
    this.props.dispatch(send());
    this.props.userList.data ? userList  : this.props.dispatch(UsersListActions.getUsersRequest());
    
  }
  render () {
    const { todos,loading,userList } = this.props;
    const filter=this.props.navigation.state.params ? this.props.navigation.state.params.filter : false ;
    
    return (
    <ScrollViewWithSpinner
      isLoading={loading}>      
          { filter?
          (<View style={styles.mainContainer}>
            <FlatList
            data={userList} 
            ListHeaderComponent={this.renderHeader}            
            keyExtractor={(item) => item.id}
            renderItem={this.renderFilterItem}
            />
          </View>):
          (<View style={styles.mainContainer}>
            <FlatList
            data={todos} 
            keyExtractor={(item) => item.id}
            renderItem={(item)=>this.renderItem(item)}
          />
          </View>)
          }
      </ScrollViewWithSpinner>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.Todos.data,
    loading: state.Todos.loading,
    userList: state.UsersList.data,

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosScreen)
