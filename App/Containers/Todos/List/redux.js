import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import {create} from 'apisauce'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getTodosRequest: null,
  getTodosSuccess: ['response'],
  getTodosFailed: ['errors']

});

export const TodosListTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loading: false,
  errors: false,
  data:{ },
  filter:false,
  filterByUsers:false
});

/* ------------- Redux Thunk ------------- */

export const send = ( ) => {
  const api = create({
    baseURL: 'https://jsonplaceholder.typicode.com/',
   })
   
  return (dispatch) => {
    dispatch(Creators.getTodosRequest());    
    return api
     .get('todos')
     .then((response) => { dispatch(Creators.getTodosSuccess(response.data))})
     .then(console.log)
  }
}
export const requestFilterTodos = ( id ) => {
  const api = create({
    baseURL: 'https://jsonplaceholder.typicode.com/',
   })
  return (dispatch) => {
    dispatch(Creators.getTodosRequest());  
    return api
     .get(`todos?userId=${id}`)
     .then((response) => { dispatch(Creators.getTodosSuccess(response.data))})
     .then(console.log)
  }
}
/* ------------- Reducers ------------- */


export const request = (state) => state.merge({
  loading: true,  
});

export const fetchSuccess = (state, { response }) => state.merge({
  data:response,
  loading: false,
});

export const fetchFailed = (state, { errors }) => state.merge({
  errors,
});



/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_TODOS_REQUEST]: request,
  [Types.GET_TODOS_SUCCESS]: fetchSuccess,
  [Types.GET_TODOS_FAILED]: fetchFailed,

});


/* ------------- Selectors ------------- */
