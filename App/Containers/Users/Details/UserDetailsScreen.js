import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView ,View} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
const Context = React.createContext();

// Styles
import styles from './UserDetailsScreenStyle'

class UserDetailsScreen extends Component {
  render () {
    const { UserDetails } = this.props;
    const dataUser= UserDetails.dataUser? UserDetails.dataUser: ' '
    return (
      <Context.Provider value={dataUser}>
         <Middleware />
      </Context.Provider>
    )
  }
}


function Middleware (props) {
  return (
    <View>
      <DetailView />
    </View>
  );
}
function DetailView (props) {

  return (
    <Context.Consumer>
      {dataUser => <ScrollView style={styles.container}>
        <View style={styles.headerContainer}>
        <Text>
          Name:
        </Text>
        <Text style={[styles.headerTitleText]}>
          {dataUser.name}
        </Text>
        <Text>
          Username:
        </Text>
        <Text style={styles.headerSubtitleText}>
          {dataUser.username}
        </Text>
        <Text>
          Email:
        </Text>
        <Text style={styles.headerSubtitleText}>
          {dataUser.email}
        </Text>
        <Text>
          Address:
        </Text>
        <Text style={styles.headerSubtitleText}>
          {dataUser.address.street}, {dataUser.address.suite}, {dataUser.address.city}
        </Text>
        <Text>
          Company:
        </Text>
        <Text style={styles.headerSubtitleText}>
          {dataUser.email}
        </Text>
      </View>
      </ScrollView>}
    </Context.Consumer>
  );
}

const mapStateToProps = (state) => {
  return {
    UserDetails:state.UserDetails
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  dispatch
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(UserDetailsScreen)
