import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  saveUser: ['item']
  
  
});

export const UsersTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loading: false,
  errors: false,
  dataUser:false,
});

/* ------------- Reducers ------------- */

export const save = (state, { item }) => state.merge({
  dataUser:item,
  loading: false,
});


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SAVE_USER]:save,
  

});


/* ------------- Selectors ------------- */
