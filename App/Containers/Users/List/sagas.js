import { call, put, select } from 'redux-saga/effects';
import UsersListActions from './redux';
import UserDetailsActions from '../Details/redux';
import { NavigationActions } from 'react-navigation';

export function* requestUsers (api ) {
   try {
      // Do the upload
      const response = yield call(api.getAllUsers);
      // Remove uploading flag if upload success
       yield put(UsersListActions.getUsersSuccess(response.data));
    } catch (error) {
      // Send error log
       yield put(UsersListActions.getUersFailed(error));
    }
  }


  export function* moveUserDetails (api,{ item} ) {
    try {
      yield put(UserDetailsActions.saveUser(item));
      
      yield put(NavigationActions.navigate({
        routeName:'UserDetailsScreen'
      }));
    } catch (error) {
      // Send error log
       
    }
}