import React, { Component } from 'react'
import { ScrollView, Image,Text, KeyboardAvoidingView,FlatList,TouchableOpacity,View } from 'react-native'
import { connect } from 'react-redux'
import UsersListActions from './redux';
import UsersItemList from '../../../Components/UserItemList'
import withSpinner from '../../../Components/WrappedPageLoading'
const ScrollViewWithSpinner = withSpinner(ScrollView);
import i18n from 'react-native-i18n';
import Images from '../../../Themes/Images'
import Colors from '../../../Themes/Colors'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './UsersScreenStyle'

class UsersScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: i18n.t('Navigation.users'),
  });


  handleClick= (item) =>{   
    this.props.dispatch(UsersListActions.moveToUserDetails(item));    
  }
  renderItem= ({item}) =>
    <UsersItemList
    name={item.name}
    userName={item.username}
    email={item.email}
    onPress={() => this.handleClick(item)}    
    />
  ;
  componentDidMount(){
    this.props.dispatch(UsersListActions.getUsersRequest());
  }

  render () {
    const { users,loading } = this.props;
    
    return (
      <ScrollViewWithSpinner
            isLoading={loading}>
        <View style={styles.mainContainer}>
            <FlatList
            data={users} 
            keyExtractor={(item) => item.id}
            renderItem={this.renderItem.bind(this)}
          />
      </View>
      </ScrollViewWithSpinner>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.UsersList.data,
    loading: state.UsersList.loading

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersScreen)
