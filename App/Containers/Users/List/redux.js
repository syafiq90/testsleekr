import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import {create} from 'apisauce'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getUsersRequest: null,
  getUsersSuccess: ['response'],
  getUsersFailed: ['errors'],
  moveToUserDetails:['item']
});

export const UsersListTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loading: false,
  errors: false,
  data:false,
  filter:false,
  filterByUsers:false
});

/* ------------- Reducers ------------- */


export const request = (state) => state.merge({
  loading: true,  
});

export const fetchSuccess = (state, { response }) => state.merge({
  data:response,
  loading: false,
});

export const fetchFailed = (state, { errors }) => state.merge({
  errors,
});



/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_USERS_REQUEST]: request,
  [Types.GET_USERS_SUCCESS]: fetchSuccess,
  [Types.GET_USERS_FAILED]: fetchFailed,
  [Types.MOVE_TO_USER_DETAILS]: null,
  
});


/* ------------- Selectors ------------- */
