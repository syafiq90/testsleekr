import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity} from 'react-native'
import styles from './Styles/UserItemListStyle'

const Provider = React.createContext();



  // // Prop type warnings
  const UserItemList= ({ name, userName, onPress, email }) => (
      <TouchableOpacity onPress={onPress}>
      <View style={[styles.container]}>
          <View style={styles.metaContainer}>
            <Text numberOfLines= {1} style= {styles.title}>
              {name}
            </Text>
            <View style= {[styles.content,{flexDirection:"column",alignItems:"flex-start", justifyContent:"flex-start"}]}>
              <Text numberOfLines= {1} style= {styles.Text}>
                {userName}
              </Text>
              <Text numberOfLines= {1} style= {styles.Text}>
                {email}
              </Text>
              </View>
          </View> 
      </View>
    </TouchableOpacity>
  );
  
  UserItemList.propTypes = {
    name: PropTypes.string,
    userName: PropTypes.string,
    onPress: PropTypes.func,
    email: PropTypes.string
  };
  
  export default UserItemList;
