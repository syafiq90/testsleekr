import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Bars } from 'react-native-loader';
import styles from './Styles/PageLoadingStyle';
import Colors from '../Themes/Colors';


const Loading = ({ size, color, fluid }) => (<View style={fluid ? styles.fluidContainer : styles.container}>
  <Bars size={size || 20} spaceBetween={10} color={color || Colors.navy} />
</View>);

Loading.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
  fluid: PropTypes.bool,
};

export default Loading;
