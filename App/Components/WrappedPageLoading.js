import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text,ScrollView } from 'react-native'
import styles from './Styles/GenralListStyle'

import PageLoading from './PageLoading';

const withSpinner = Comp => ({ isLoading, children, ...props }) => {
  if (isLoading) {
    return   <PageLoading />
  } else {
    return (
      <Comp {...props}>
        {children}
      </Comp>
    )
  }
};

export default withSpinner;
