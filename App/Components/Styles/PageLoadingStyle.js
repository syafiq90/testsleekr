import { StyleSheet } from 'react-native';
import Metrics from '../../Themes/Metrics';
import Colors from '../../Themes/Colors';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    zIndex: 3001,
    width: Metrics.screenWidth,
    height: Metrics.screenHeight,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  fluidContainer: {
    width: Metrics.screenWidth,
    justifyContent: 'center',
    alignItems: 'center',
  },
});