import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text } from 'react-native';
import styles from './Styles/TabBarIconStyle';

const TabBarIcon = ({ source, tintColor, label }) => (<View style={styles.container}>
  <Image
    source={source}
    style={[styles.icon, { tintColor: tintColor }]}
  />
  <Text style={{ color: tintColor }}>{label}</Text>
</View>);

TabBarIcon.propTypes = {
  source: PropTypes.any,
  tintColor: PropTypes.string,
  label: PropTypes.string,
};

export default TabBarIcon;
