import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity} from 'react-native'
import styles from './Styles/UserItemListStyle'


  // // Prop type warnings
  const FilterByUsers= ({ name, onPress, email }) => (
      <TouchableOpacity onPress={onPress}>
      <View style={[styles.container]}>
          <View style={styles.metaContainer}>
            <Text numberOfLines= {1} style= {styles.title}>
              {name}
            </Text>
          </View> 
      </View>
    </TouchableOpacity>
  );
  
  FilterByUsers.propTypes = {
    name: PropTypes.string,
    onPress: PropTypes.func,
  };
  
  export default FilterByUsers;
