import React from 'react';
import { TabNavigator, TabBarBottom } from 'react-navigation';
import i18n from 'react-native-i18n';
import TabBarIcon from '../Components/TabBarIcon';
import AlbumsScreen from '../Containers/Albums/List/AlbumsScreen';
import PostsScreen from '../Containers/Posts/List/PostsScreen';
import UsersScreen from '../Containers/Users/List/UsersScreen';
import TodosScreen from '../Containers/Todos/List/TodosScreen';
import { Images } from '../Themes/index';

const tabOptions = {
  tabBarComponent: TabBarBottom,
  tabBarPosition: 'bottom',
  animationEnabled: true,
  swipeEnabled: true,
  tabBarOptions: {
    activeTintColor: '#02B875',
    showLabel: false,
    style: {
      height: 60,
    }
  },
  initialRouteName : 'PostsScreen',
  
};


export default MainNavigation = TabNavigator({
  PostsScreen: {
    screen: PostsScreen,
    navigationOptions: {
      tabBarLabel: i18n.t("Navigation.posts"),
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon source={Images.postsIcon} tintColor={tintColor} label={i18n.t("Navigation.posts")} />
      ),
    }
  },
  AlbumsScreen: {
    screen: AlbumsScreen,
    navigationOptions: {
      tabBarLabel: i18n.t("Navigation.albums"),
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon source={Images.albumsIcon} tintColor={tintColor} label={i18n.t("Navigation.albums")} />
      ),
    }
  },
  TodosScreen: {
    screen: TodosScreen,
    navigationOptions: {
      tabBarLabel: i18n.t("Navigation.todos"),
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon source={Images.todoIcon} tintColor={tintColor} label={i18n.t("Navigation.todos")} />
      ),
    }
  },
  UsersScreen: {
    screen: UsersScreen,
    navigationOptions: {
      tabBarLabel: i18n.t("Navigation.users"),
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon source={Images.userProfileIcon} tintColor={tintColor} label={i18n.t("Navigation.users")} />
      ),
    }
  },
  
}, tabOptions);

 
