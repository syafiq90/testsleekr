import { StackNavigator } from 'react-navigation'
import TodosScreen from '../Containers/Todos/List/TodosScreen'
import PhotosScreen from '../Containers/Albums/Photos/PhotosScreen'
import AlbumsScreen from '../Containers/Albums/List/AlbumsScreen'
import PostDetailsScreen from '../Containers/Posts/Details/PostDetailsScreen'
import PostsScreen from '../Containers/Posts/List/PostsScreen'
import UserDetailsScreen from '../Containers/Users/Details/UserDetailsScreen'
import UsersScreen from '../Containers/Users/List/UsersScreen'
import LaunchScreen from '../Containers/LaunchScreen'
import MainTabNavigator from '../Navigation/MainTabNavigator'
import i18n from 'react-native-i18n';


import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({

  TodosScreen: { 
    screen: TodosScreen ,
  },

  PhotosScreen: { 
    screen: PhotosScreen,
    navigationOptions: {
      title: i18n.t('Navigation.albumsDetails')
    }, 
  },
  AlbumsScreen: { 
    screen: AlbumsScreen,
  },

  PostDetailsScreen: { 
    screen: PostDetailsScreen,
    navigationOptions: {
      title: i18n.t('Navigation.postsDetails')
    }, 
  },
  PostsScreen: { 
    screen: PostsScreen,
  },

  UserDetailsScreen: { 
    screen: UserDetailsScreen,
    navigationOptions: {
      title: i18n.t('Navigation.usersDetails')
    }, 
  },
  UsersScreen: { 
    screen: UsersScreen,
  },


  
  MainTabNavigator: { 
    screen: MainTabNavigator,
    header:'none'
  }


  //TabNavigator


}, {
  // Default config for all screens
  initialRouteName: 'MainTabNavigator',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
